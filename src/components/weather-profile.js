import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

import { COLORS, FONTS } from "../constants";
import { convertTimestampToDate } from "../utils";

const WeatherProfile = ({ details }) => {
  const { dt, weather, temp: temperature } = details;

  const weatherDay = convertTimestampToDate(dt, "dddd, DD MMMM YYYY");
  const weatherIcon = weather[0].icon;
  const weatherDescription = weather[0].main;

  return (
    <View style={styles.container}>
      <Text
        style={{
          fontFamily: FONTS.WEIGHT[500],
          fontSize: 18,
          color: COLORS.GRAY[800],
        }}
      >
        {weatherDay}
      </Text>
      <View style={styles.weatherInfo}>
        <Image
          style={styles.weatherIcon}
          source={{
            uri: `https://openweathermap.org/img/wn/${weatherIcon}@2x.png`,
          }}
        />
        <View style={{ flexDirection: "column" }}>
          <Text
            style={{
              fontFamily: FONTS.WEIGHT[500],
              fontSize: 16,
              color: COLORS.GRAY[700],
            }}
          >
            {weatherDescription}
          </Text>
          <Text
            style={{
              fontFamily: FONTS.WEIGHT[500],
              fontSize: 28,
              color: COLORS.GRAY[700],
            }}
          >
            {temperature}°C
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: 312,
    maxHeight: 160,
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    paddingVertical: 12,
    paddingHorizontal: 16,
    marginBottom: 16,
    borderWidth: 1,
    borderColor: COLORS.GRAY[200],
  },
  weatherInfo: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginTop: 8,
  },
  weatherIcon: {
    width: 100,
    height: 100,
  },
});

export default WeatherProfile;
