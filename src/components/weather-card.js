import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

import { COLORS, FONTS } from "../constants";
import { convertTimestampToDate } from "../utils";

const WeatherCard = ({ details }) => {
  const { dt, weather, temp: temperature } = details;

  const weatherDay = convertTimestampToDate(dt, "dddd");
  const weatherIcon = weather[0].icon;
  const weatherDescription = weather[0].main;

  return (
    <View style={styles.container}>
      <Image
        style={styles.weatherIcon}
        source={{
          uri: `https://openweathermap.org/img/wn/${weatherIcon}@2x.png`,
        }}
      />
      <View style={styles.weatherDay}>
        <Text
          style={{
            fontFamily: FONTS.WEIGHT[400],
            fontSize: 14,
            color: COLORS.GRAY[500],
          }}
        >
          {weatherDay}
        </Text>
        <Text
          style={{
            fontFamily: FONTS.WEIGHT[500],
            fontSize: 16,
            color: COLORS.GRAY[700],
          }}
        >
          {weatherDescription}
        </Text>
      </View>
      <View style={styles.weatherTemperature}>
        <Text
          style={{
            fontFamily: FONTS.WEIGHT[500],
            fontSize: 18,
            color: COLORS.GRAY[700],
          }}
        >
          {temperature.day}°C
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    maxWidth: 312,
    maxHeight: 120,
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    paddingVertical: 12,
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  weatherIcon: {
    width: 88,
    height: 88,
  },
  weatherDay: {
    flex: 1,
    height: 64,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
  },
  weatherTemperature: {
    width: 88,
    height: 88,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default WeatherCard;
