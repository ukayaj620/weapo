import axios from "axios";
import { useEffect, useState } from "react";

import { CONFIG } from "../constants";

export const useWeather = (location) => {
  const [weatherDetails, setWeatherDetails] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    if (location) {
      (async () => {
        const { coords } = location;
        const { longitude, latitude } = coords;

        try {
          const { data } = await axios.get(
            `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=minutely,hourly&units=metric&appid=${CONFIG.API_KEY}`
          );
          setWeatherDetails(data);
        } catch (error) {
          setError(error);
        }
      })();
    }
  }, [location]);

  return { weatherDetails, error };
};
