import { useEffect, useState } from "react";
import * as Location from "expo-location";

export const useGeolocation = () => {
  const [location, setLocation] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    (async () => {
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setError("Permission to access location was denied");
        return;
      }

      const location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  return { location, error };
};
