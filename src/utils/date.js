import moment from "moment";

export const convertTimestampToDate = (timestamp, dateFormat) => {
  return moment(new Date(timestamp * 1000)).format(dateFormat);
};
