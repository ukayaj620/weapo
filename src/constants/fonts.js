export const FONTS = {
  WEIGHT: {
    400: "Inter_400Regular",
    500: "Inter_500Medium",
    700: "Inter_700Bold",
  },
};
