import React from "react";
import { StatusBar } from "expo-status-bar";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import {
  useFonts,
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold,
} from "@expo-google-fonts/inter";
import AppLoading from "expo-app-loading";

import { WeatherCard, WeatherProfile } from "./src/components";
import { useGeolocation, useWeather } from "./src/hooks";

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold,
  });

  const { location, error: geolocationError } = useGeolocation();
  const { weatherDetails, error: weatherError } = useWeather(location);

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  if (geolocationError || weatherError) {
    alert("Location or Weather can't be detected!");
    return null;
  }

  if (!location || !weatherDetails) {
    return <AppLoading />;
  }

  const { current, daily } = weatherDetails;

  const renderWeatherCards = () => {
    return daily.slice(1).map((weather, index) => {
      return <WeatherCard key={`#weather-card-${index}`} details={weather} />;
    });
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={styles.container}
      contentContainerStyle={{ alignItems: "center" }}
    >
      <WeatherProfile details={current} />
      {renderWeatherCards()}
      <StatusBar style="auto" />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    marginTop: 32,
  },
});
